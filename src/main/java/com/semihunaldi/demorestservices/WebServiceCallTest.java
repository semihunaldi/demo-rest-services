package com.semihunaldi.demorestservices;

import com.google.gson.Gson;
import com.semihunaldi.demorestservices.model.UserResponse;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by semihunaldi on 6.11.2018
 */
public class WebServiceCallTest {

	public static void main(String[] args) {
		try {

			URL url = new URL("http://localhost:8080/UserRestService/getUsers");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				Gson gson = new Gson();
				UserResponse[] userResponses = gson.fromJson(output, UserResponse[].class);
				userResponses.toString();
			}

			conn.disconnect();

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
	}
}
