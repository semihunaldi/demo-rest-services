package com.semihunaldi.demorestservices.rest;

import com.semihunaldi.demorestservices.model.UserResponse;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by semihunaldi on 6.11.2018
 */

@RestController
@RequestMapping(value = "/UserRestService")
public class UserWebServiceController {

	@RequestMapping(value = "/saveUser", method = {RequestMethod.POST})
	public List<UserResponse> saveUser(@RequestBody UserResponse request) {
		List<UserResponse> userResponses = prepareUsers();
		userResponses.add(request);
		return userResponses;
	}

	@RequestMapping(value = "/getUserByEmail", method = {RequestMethod.GET})
	public List<UserResponse> getUserByEmail(@RequestParam(required = false) String email) {
		List<UserResponse> userResponses = prepareUsers();
		if(email == null && email.isEmpty()) {
			throw new RuntimeException("Tuttuk");
		}
		return userResponses.stream().filter(userResponse -> userResponse.getEmail().equals(email)).collect(Collectors.toList());
	}

	@RequestMapping(value = "/getUsers", method = {RequestMethod.GET})
	public List<UserResponse> getUsers() {
		return prepareUsers();
	}

	@RequestMapping(value = "/test", method = {RequestMethod.GET})
	public List<UserResponse> test() {
		return prepareUsers();
	}

	private List<UserResponse> prepareUsers() {
		List<UserResponse> userResponses = new ArrayList<>();
		UserResponse userResponse = new UserResponse();
		userResponse.setEmail("semihunaldi@gmail.com");
		userResponse.setActive(true);
		userResponse.setAge(27);
		userResponse.setUserName("semihunaldi");
		UserResponse userResponse2 = new UserResponse();
		userResponse2.setEmail("egemert@gmail.com");
		userResponse2.setActive(true);
		userResponse2.setAge(25);
		userResponse2.setUserName("egemert");
		userResponses.add(userResponse2);
		userResponses.add(userResponse);
		return userResponses;
	}
}
