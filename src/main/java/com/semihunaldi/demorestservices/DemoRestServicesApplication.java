package com.semihunaldi.demorestservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by semihunaldi on 6.11.2018
 */

@SpringBootApplication
public class DemoRestServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoRestServicesApplication.class, args);
	}
}
